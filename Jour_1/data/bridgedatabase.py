import os
import pandas as pd
import re
import numbers
import logging

class BridgeDataBase:
    """
    A class used to represent a Bridge Database.

    ...

    Attributes
    ----------
    root : str
        the root directory
    data_path : str
        the path to the bridge data files
    xlsfiles : list
        a list of Excel file names in the data path
    data : dict
        a dictionary to hold the bridge data from Excel files

    Methods
    -------
    get_files():
        Gets all Excel files from the data path.
    load_data():
        Loads data from Excel files into a dictionary.
    stats():
        Prints the number of loaded files.
    process_data(data):
        Processes bridge data, filtering for numeric values greater than zero.
    extract_bridge(num):
        Extracts bridge information from raw data.
    get_bridges():
        Gets all bridges data.
    """

    def __init__(self):
        """
        Initializes BridgeDataBase with directory details and empty data containers.

        Raises
        ------
        FileNotFoundError
            If the specified data_path doesn't exist
        """
        self.root = os.getcwd()
        self.data_path = os.path.abspath(
            os.path.join(self.root,
                         'confidential_Systra_database',
                         'ponts_ferroviaires')
        )

        # Check if data_path exists, if not raise an error
        if not os.path.isdir(self.data_path):
            raise FileNotFoundError(f"The specified data directory {self.data_path} does not exist.")

        self.xlsfiles = []
        self.data = {}

    def get_files(self):
        """
        Gets all Excel files from the data path.
        
        Populates the xlsfiles attribute with all filenames ending with '.xlsx' in the data path.
        """
        self.xlsfiles = [f for f in os.listdir(self.data_path)
                         if os.path.isfile(os.path.join(self.data_path, f)) and f.endswith('.xlsx')]

    def load_data(self):
        """
        Loads data from Excel files into a dictionary.
        
        For each file in xlsfiles, reads the Excel file into a DataFrame and stores it in the data dictionary.
        """
        for sheet in self.xlsfiles:
            filename = os.path.join(self.data_path, sheet)
            logging.info('Reading file %s', sheet)
            self.data[sheet] = pd.read_excel(filename, header=None)


    def stats(self):
        """Prints the number of loaded files."""
        print('Number of files loaded: {}'.format(len(self.data)))

    @staticmethod
    def process_data(data):
        """
        Processes bridge data, filtering for numeric values greater than zero.

        Parameters
        ----------
        data : Series
            The raw data to be processed.

        Returns
        -------
        list
            A list of processed data points.
        """
        processed_data = []
        for s in data:
            if isinstance(s, numbers.Number) and s > 0:
                processed_data.append(s)
        return processed_data

    def len(self):
        """
        The number of loaded bridge data files.

        Returns
        -------
        int
            The number of loaded bridge data files.
        """
        return len(self.data)

    def extract_bridge(self, num):
        """
        Extracts bridge information from raw data.

        Parameters
        ----------
        num : int
            The index of the bridge file to extract data from.

        Returns
        -------
        dict
            A dictionary of processed bridge data with the following keys:
                'filename': the filename of the Excel file for the bridge
                'type': the type of the bridge extracted from the filename
                'length': the length of the bridge
                'height_prof': the height profile of the bridge, a list of heights
                'travelage': the travelage of the bridge, a list of cumulative distances
                'obstacle_start': a list of starting points of obstacles on the bridge
                'obstacle_end': a list of ending points of obstacles on the bridge
        """
        filename = self.xlsfiles[num]
        bridge_raw = self.data[filename]
        bridge_raw.columns = ['length', 'profile', 'obstacles', 'travelage']
        bridge = {'filename': filename, 'type': re.search('(PF.*)\.xlsx', filename).group(1)}
        bridge['length'] = bridge_raw['length'][5]
        bridge['height_prof'] = self.process_data(bridge_raw['profile'][5:])
        travelage = self.process_data(bridge_raw['travelage'])
        for i in range(len(travelage) - 1):
            travelage[i + 1] += travelage[i]
        bridge['travelage'] = travelage
        obstacle_start = []
        obstacle_end = []
        inside_obstacle = False
        for i, s in enumerate(bridge_raw['obstacles']):
            if isinstance(s, numbers.Number) and s > 0:
                if not inside_obstacle:
                    inside_obstacle = True
                    obstacle_start.append(s)
                else:
                    inside_obstacle = False
                    obstacle_end.append(s)
        bridge['obstacle_start'] = obstacle_start
        bridge['obstacle_end'] = obstacle_end
        return bridge

    def get_bridges(self):
        """
        Gets all bridges data.

        This method executes the full process of gathering and processing bridge data:
        1. Gets filenames from the data path.
        2. Loads data from the Excel files.
        3. Prints the number of loaded files.
        4. Processes each file to extract bridge data.

        Returns
        -------
        list
            A list of dictionaries containing bridge data.
        """
        logging.info('Get bridge files...')
        self.get_files()
        logging.info('Done.')
        logging.info('Loading data...')
        self.load_data()
        logging.info('Done.')
        self.stats()
        bridge_list = [self.extract_bridge(i) for i in range(len(self.data))]
        logging.info('Finished extracting bridge data.')
        return bridge_list
