#!/usr/bin/env python3
"""
Read data and do some graphical representation
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import bridgedatabase

from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import silhouette_score
from sklearn.cluster import DBSCAN
from sklearn.metrics import adjusted_rand_score
from sklearn.decomposition import PCA

######################################
#
# Get data
#
######################################

# Create bdb object

bdb = bridgedatabase.BridgeDataBase()

# Get bridge list in the database

bridge_list = bdb.get_bridges()
num_bridges = bdb.len()

######################################
#
# Examples of use
#
######################################

######################################
#
# Print the keys of bridge #0
#
######################################

bridge_list[0].keys()

############################################################
#
# Print the length of bridge #1 and plot the height profile
#
############################################################

print("---")
ind = 2
print("Bridge #{0}".format(ind))
bridge = bridge_list[ind]
length = bridge["length"]
z = np.array(bridge["height_prof"])
n = z.shape[0]
x = np.linspace(0, length, n)
t = np.array(bridge["travelage"])
"""
print("Bridge length = {0} m".format(length))
print("Number of points in height profile = {0}".format(n))

plt.plot(x, -z)
plt.xlabel("x")
plt.ylabel("height")
bottom, top = plt.ylim()
for i in range(t.shape[0]):
    plt.plot([t[i], t[i]], [bottom, top], "m")

plt.show()


######################################
#
# Retrieve the list of lengths
#
######################################

b_lengths = []
for i in range(num_bridges):
    b_lengths.append(bridge_list[i]["length"])

# Histogram of the lengths

num_bins = 30
fig, ax = plt.subplots()
n, bins, patches = ax.hist(b_lengths, num_bins)
plt.title("histogram of lengths")
plt.xlabel("length")
plt.ylabel("frequency")
fig.tight_layout()
plt.show()
"""
######################################
#
# Number of bridges of each type
#
######################################

types_stats = {}
for i in range(num_bridges):
    type = bridge_list[i]["type"]
    if type in types_stats:
        types_stats[type] += 1
    else:
        types_stats[type] = 1

# sort values
# (see https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value)
# and display frequencies

types_stats_sorted = {
    k: v for k, v in sorted(types_stats.items(), key=lambda item: item[1], reverse=True)
}
"""
plt.bar(range(len(types_stats)), types_stats_sorted.values())
plt.xticks(range(len(types_stats)), types_stats_sorted.keys())
plt.show()
"""
#############################################################################
#
# Convert types to integers values and retrieve some stats in an np ndarray
#
#############################################################################

X = np.ndarray([num_bridges, 5])
k=0
L=[]
for i in range(num_bridges):
    type = bridge_list[i]["type"]
    X[i, 0] = int(type[2:])
    X[i, 1] = bridge_list[i]["length"]
    X[i, 2] = np.min(np.array(bridge_list[i]["height_prof"]))
    X[i, 3] = np.max(np.array(bridge_list[i]["height_prof"]))
    X[i, 4] = len(bridge_list[i]["travelage"])

pca = PCA(n_components=2).fit(X[:,1:5])
data2D = pca.transform(X[:,1:5])


"""
silhouette_coeff = []

for k in range(2,30):
    kmeans = KMeans(n_clusters=k, **kmeans_kwargs)
    kmeans.fit(scaled_X)
    score = silhouette_score(scaled_X, kmeans.labels_)
    silhouette_coeff.append(score)
    
    
plt.style.use("fivethirtyeight")
plt.plot(range(2,30), silhouette_coeff)
plt.xticks(range(2,30))
plt.xlabel("Number of Clusters")
plt.ylabel("Siloette Coefficient")
plt.show()
"""
scaler = MinMaxScaler()
data2D = scaler.fit_transform(data2D)
kmeans = KMeans(n_clusters=14, init="k-means++",max_iter=300, n_init=20)
kmeans.fit(data2D)
plt.scatter(data2D[:,0],data2D[:,1],c=kmeans.labels_,cmap='rainbow')
plt.show()
"""

fig,ax=plt.subplots()
for i  in range(21):
    indices = np.where(X[:, 0] == i)
    #plt.scatter(X[indices[0], 1], X[indices[0], 3])
    H,L=np.ndarray.mean(X[indices[0], 3]),np.ndarray.mean(X[indices[0], 1])
    ax.scatter(L,H,marker='x')
    ax.annotate("PF"+str(i+1), (L,H))

plt.xlabel("length (m)")
plt.ylabel("max height (m)")
plt.show()


##################################################
#
# correlation longueur - travelage
#
##################################################

plt.plot(X[:, 1], X[:, 3], ".")
plt.xlabel("length")
plt.ylabel("max height")
plt.show()

##################################################
#
# stats by type
#
##################################################


# retrieve the types
types = np.unique(X[:, 0])
types_number = len(types)

l_list = []
t_list = []
h_list = []
for type in types:
    indices = np.where(X[:, 0] == type)
    lengths = X[indices[0], 1].ravel()
    heights = X[indices[0], 3].ravel()
    travelages = X[indices[0], 4].ravel()
    l_list.append(lengths)
    t_list.append(travelages)
    h_list.append(heights)


fig = plt.figure(figsize=(10, 7))
ax = fig.add_subplot(311)
plt.boxplot(l_list)
ax.set_xticklabels(["PF{0:n}".format(type) for type in types])
plt.ylabel("length")

ax = fig.add_subplot(312)
ax.boxplot(t_list)
ax.set_xticklabels(["PF{0:n}".format(type) for type in types])
plt.ylabel("travelage")

ax = fig.add_subplot(313)
ax.boxplot(h_list)
ax.set_xticklabels(["PF{0:n}".format(type) for type in types])
plt.ylabel("height")

plt.show()

##################################################
#
# scatter plots
#
##################################################

type0 = 1
type1 = 2

ind0 = np.where(X[:, 0] == type0)
ind1 = np.where(X[:, 0] == type1)

X0 = X[ind0[0], :]
X1 = X[ind1[0], :]

Xc = np.vstack((X0, X1))
Xc[:, 4] = Xc[:, 3] - Xc[:, 2]

df = pd.DataFrame(Xc)
df.columns = ["type", "length", "h_min", "h_max", "delta"]

sns.pairplot(df, hue="type")
plt.show()
"""